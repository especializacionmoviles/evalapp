package com.andrea.evalapp.Controlador

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.andrea.evalapp.R

class splashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({

            val intent = Intent (this, MapsActivity::class.java)
            startActivity(intent)

        }, 3000)
    }
}
