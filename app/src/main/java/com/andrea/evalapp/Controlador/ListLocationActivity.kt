package com.andrea.evalapp.Controlador

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import com.andrea.evalapp.Modelo.Constantes
import com.andrea.evalapp.Modelo.GestorBd
import com.andrea.evalapp.R
import kotlinx.android.synthetic.main.activity_list_location.*

class ListLocationActivity : AppCompatActivity() {
    internal var conexion = GestorBd(this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_location)

        var list1: ArrayList<String> = conexion.mostrarUbicacion()
        var list2: ArrayList<String> = conexion.mostrarID()
        var adp1: ArrayAdapter<String>
        var adp2: ArrayAdapter<String>
        adp1 = ArrayAdapter(this,android.R.layout.simple_list_item_1,list1)
        adp2 = ArrayAdapter(this,android.R.layout.simple_list_item_1,list2)

        listUbi.adapter = adp1
        listID.adapter = adp2

        btnEliminar.setOnClickListener {
            val ubi =  GestorBd(this)
            val bd = ubi.writableDatabase
            val cant = bd.delete(Constantes.TABLE_NAME, "id=${edtID.text.toString()}", null)
            bd.close()
            if (cant == 1)
                Toast.makeText(this, "Se borró la ubicación con ese ID", Toast.LENGTH_SHORT).show()
            else
                Toast.makeText(this, "No existe una ubicación con ese ID", Toast.LENGTH_SHORT).show()

            val intent = Intent(this, ListLocationActivity::class.java)
            startActivity(intent)
        }

        btnAtras.setOnClickListener {
            val intent = Intent(this, MapsActivity::class.java)
            startActivity(intent)
        }
    }
}
