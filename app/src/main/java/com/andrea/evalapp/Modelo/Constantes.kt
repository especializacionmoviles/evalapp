package com.andrea.evalapp.Modelo

/**
 * Created by Andrea on 05/05/2018.
 */
public class Constantes {
    companion object {
        val DATABASE_NAME = "Ubicaciones"
        val TABLE_NAME = "ubication"
        val COL_UBI = "ubicacion"
        val COL_ID = "id"
        val VERSION = 1

        val table = "CREATE TABLE " + TABLE_NAME + "(" +
                COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COL_UBI + " TEXT)"
    }
}