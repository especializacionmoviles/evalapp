package com.andrea.evalapp.Modelo

/**
 * Created by Andrea on 05/05/2018.
 */
public class Ubicacion {
    var id: Int = 0
    var ubicacion: String = " "

    constructor(id: Int, ubicacion: String){
        this.id=id
        this.ubicacion=ubicacion
    }
}