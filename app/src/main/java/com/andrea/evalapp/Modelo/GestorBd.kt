package com.andrea.evalapp.Modelo

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

/**
 * Created by Andrea on 05/05/2018.
 */
public class GestorBd (context: Context): SQLiteOpenHelper(context, Constantes.DATABASE_NAME, null, Constantes.VERSION) {
    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(Constantes.table)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun mostrarUbicacion(): ArrayList<String>{
        var list1 : ArrayList<String> = arrayListOf()
        val db: SQLiteDatabase = readableDatabase
        var c = db.rawQuery("Select * from ubication", null)
        while (c.moveToNext()){
            var ubicacion : String = c.getString(c.getColumnIndex("ubicacion"))
            list1.add(ubicacion)
        }
        return list1
    }
    fun mostrarID(): ArrayList<String>{
        var list2 : ArrayList<String> = arrayListOf()
        val db: SQLiteDatabase = readableDatabase
        var c = db.rawQuery("Select * from ubication", null)
        while (c.moveToNext()){
            var id : String = c.getString(c.getColumnIndex("id"))
            list2.add(id)
        }
        return list2
    }
}